import "./TodoInput.css";

const TodoInput = ({ todo, setTodo, addTodo }) => {
  return (
    <div className="to-do-input">
      <input
        type="text"
        name="todo"
        value={todo.title}
        placeholder="Create a new todo"
        onChange={(e) => {
          setTodo({ ...todo, title: e.target.value });
        }}
      />

      <button className="add-button" onClick={addTodo}>
        Add a ToDo
      </button>
    </div>
  );
};

export default TodoInput;
