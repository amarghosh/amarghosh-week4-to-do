import { useState, useEffect } from "react";
import TodoInput from "./TodoInput";
import TodoList from "./TodoList";
import "./App.css";
import Header from "./Header";
import { get, put, del } from "aws-amplify/api";
import { Amplify } from "aws-amplify";
import awsconfig from "./aws-exports";
import { withAuthenticator } from "@aws-amplify/ui-react";
import "@aws-amplify/ui-react/styles.css";
Amplify.configure(awsconfig);
const App = ({ signOut, user }) => {
  const API_NAME = "amarghoshweek4todo";
  const API_PATH = "/todos"; // creating local state variable, and it's updater function, using useState hook.
  const [todos, setTodos] = useState([]);
  const [todo, setTodo] = useState({ id: 0, title: "" });
  async function getTodos() {
    try {
      const restOperation = await get({
        apiName: API_NAME,
        path: API_PATH,
      });
      const response = await restOperation.response;
      if (response.statusCode === 200) {
        console.log("GET call succeeded");
        const data = await response.body.json();
        console.log("GET call succeded: " + JSON.stringify(data));
        setTodos(data);
      } else {
        console.log(
          "GET call failed: Non 200 status code: ",
          response.statusCode
        );
      }
    } catch (error) {
      console.log("GET call failed: ", error);
    }
  }
  const addTodo = async () => {
    if (todo != null) {
      todo.id = Date.now();
      setTodos([...todos, todo]);
      console.log("AddToDo: " + JSON.stringify(todo));
      try {
        const restOperation = await put({
          apiName: API_NAME,
          path: API_PATH,
          options: {
            body: todo,
          },
        });
        const response = await restOperation.response;
        if (response.statusCode === 200) {
          console.log("ADD call succeeded");
        } else {
          console.log(
            "ADD call failed: Non 200 status code: ",
            response.statusCode
          );
        }
      } catch (err) {
        console.log("ADD call failed: ", err);
      }
    }
  };
  const deleteTodo = async (id) => {
    try {
      const deleteIndex = todos.findIndex((todo) => todo.id === id);
      setTodos(todos.toSpliced(deleteIndex, 1));
      const restOperation = await del({
        apiName: API_NAME,
        path: API_PATH + "/object/" + id,
      });
      const response = await restOperation.response;
      if (response.statusCode === 200) {
        console.log("DELETE call succeeded");
      } else {
        console.log(
          "DELETE call failed: Non 200 status code: ",
          response.statusCode
        );
      }
    } catch (error) {
      console.log("DELETE call failed: ", error);
    }
  };
  useEffect(() => {
    getTodos();
  }, []);
  return (
    <div>
      {" "}
      <div className="App">
        <Header />
        <TodoInput todo={todo} setTodo={setTodo} addTodo={addTodo} />
        <TodoList list={todos} remove={deleteTodo} /> Version: 1.2{" "}
      </div>
      <button onClick={signOut}>Sign out</button>{" "}
    </div>
  );
};
export default withAuthenticator(App);
