import React from "react";

import "./TodoList.css";

const TodoList = ({ list, remove }) => {
  return (
    <>
      {list?.length > 0 ? (
        <div className="to-do-list">
          {list.map((entry, index) => (
            <div key={index} className="item">
              <p> {entry.title} </p>

              <button
                className="delete-button"
                onClick={() => {
                  remove(entry.id);
                }}
              >
                Delete
              </button>
            </div>
          ))}
        </div>
      ) : (
        <div className="empty">
          <p>No task found.</p>
        </div>
      )}
    </>
  );
};

export default TodoList;
